﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ExtensionsPack
{
    public static class TelephoneParserExtensions
    {
        public static bool IsStringTelephoneNumber(this String targetString)
        {
            bool result = false;
            Regex expr = new Regex(@"\d{3}\s\d{7}", RegexOptions.CultureInvariant);
            result = expr.IsMatch(targetString);
            return result;
        }
        public static bool IsStringTelephoneNumber(this String targetString, string pattern)
        {
            bool result = false;
            Regex expr = new Regex(pattern, RegexOptions.CultureInvariant);
            result = expr.IsMatch(targetString);
            return result;
        }
    }
}
