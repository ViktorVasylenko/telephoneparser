﻿using ExtensionsPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerNamespace
{
    public class Manager
    {
        public bool CheckStringAsTelephoneNumber(string target)
        {
            bool result = false;

            result = target.IsStringTelephoneNumber();

            return result;
        }
        public bool CheckStringAsTelephoneNumber(string target, string pattern)
        {
            bool result = false;

            result = target.IsStringTelephoneNumber(pattern);

            return result;
        }
    }
}
