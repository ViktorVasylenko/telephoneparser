﻿using ManagerNamespace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelephoneParser
{
    class Program
    {
        static void Main(string[] args)
        {
            Manager manager = new Manager();
            string userInput, userPattern;
            string pattern;
            bool stringMatch;

            Console.WriteLine("Enter string as telephone number pattern 'ddd ddddddd'");
            userInput = Console.ReadLine();
            stringMatch = manager.CheckStringAsTelephoneNumber(userInput);
            if (stringMatch)
                Console.WriteLine("Enter is correct!");
            else
                Console.WriteLine("Enter is incorrect!");
            Console.ReadKey(true);


            Console.WriteLine("Enter string for pattern");
            userPattern = Console.ReadLine();
            Console.WriteLine("Enter string as telephone number pattern '{0}'", userPattern);
            userInput = Console.ReadLine();
            stringMatch = manager.CheckStringAsTelephoneNumber(userInput, userPattern);
            if (stringMatch)
                Console.WriteLine("Enter is correct!");
            else
                Console.WriteLine("Enter is incorrect!");
            Console.ReadKey(true);
        }
    }
}
